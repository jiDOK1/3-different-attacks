using UnityEngine;

public class TestSlash : MonoBehaviour
{
    Animator anim;
    int numClicked;
    float resetDelay = 3f;
    float clickTimer;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (clickTimer >= 0f)
        {
            clickTimer += Time.deltaTime;
        }

        if (clickTimer >= resetDelay)
        {
            clickTimer = -1f;
            numClicked = 0;
        }

        if (Input.GetMouseButtonDown(0))
        {
            anim.SetTrigger("slash");
            anim.SetInteger("slashIdx", numClicked);
            numClicked++;
            numClicked %= 3;
            clickTimer = 0f;
        }
    }
}
